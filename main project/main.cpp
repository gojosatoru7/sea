﻿#include <iostream>
#include <iomanip>

using namespace std;

#include "Conference.h"
#include "author_filer.h"
#include "constants.h"
#include "filter.h"


void output(Conference* subscription)
{

	cout << "Author...........: ";
	cout << subscription->author.last_name << " ";
	cout << subscription->author.first_name[0] << ". ";
	cout << subscription->author.middle_name[0] << ".";
	cout << "   ";
	cout << '"' << subscription->topic << '"';
	cout << '\n';
	cout << "Start time.....: ";
	cout << setw(2) << setfill('0') << subscription->start.hours << ':';
	cout << setw(2) << setfill('0') << subscription->start.minutes;
	cout << '\n';
	cout << "End time...: ";
	cout << setw(2) << setfill('0') << subscription->finish.hours << ':';
	cout << setw(2) << setfill('0') << subscription->finish.minutes;
	cout << '\n';
	cout << '\n';

}

int main()
{
	cout << "14z\n";
	cout << "Laboratory work #1. GIT\n";
	cout << "Variant #2. Conference progra\n";
	cout << "Author: Sasha Bazhenova\n\n";
	Conference* subscriptions[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", subscriptions, size);
		cout << "***** Conference programm *****\n\n";
		for (int i = 0; i < size; i++)


		{
			output(subscriptions[i]);
		}
		bool (*check_function)(Conference*) = NULL;

		cout << "\nSelect data filtering:\n";
		cout << "1)Output all the reports of Ivan Ivanovich Ivanov.\n";
		cout << "2)Output all reports longer than 15 minutes.\n";	
		cout << "\n   : ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = sea_by_author; //       
			cout << "Autor:\n\n";
			break;
		case 2:
		{
			check_function = sea_by_date; //       
			cout << "Time:\n\n";
			break;
		break;
		}
		default:
			throw "  ";
		}
		if (check_function)
		{
			int new_size;
			Conference** filtered = filter(subscriptions, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete subscriptions[i];
		}
	}


	catch (const char* error)
	{
		cout << error << '\n';
	}

	return 0;
}
