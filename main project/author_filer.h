#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "Conference.h"

void read(const char* file_name, Conference* array[], int& size);

#endif
