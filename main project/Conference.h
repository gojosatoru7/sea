#pragma once
#ifndef CONFERENCE
#define CONFERENCE

#include "constants.h"

struct date
{
    int hours;
    int minutes;

};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct Conference
{
    person author;
    date start;
    date finish;
    char topic[MAX_STRING_SIZE];
};

#endif