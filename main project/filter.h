#pragma once
#ifndef FILTER_H
#define FILTER_H

#include "Conference.h"

Conference**filter(Conference* array[], int size, bool (*check)(Conference* element), int& result_size);



bool sea_by_author(Conference* element);



bool sea_by_date(Conference* element);



#endif

