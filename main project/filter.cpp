﻿#include "filter.h"
#include <cstring>
#include <iostream>
#include "filter.h"
#include <cstring>
#include <iostream>

Conference** filter(Conference* array[], int size, bool (*check)(Conference* element), int& result_size)
{
	Conference** result = new Conference * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool sea_by_author(Conference* element)
{
	return strcmp(element->author.first_name, "Ivanov I. I.") == 0 &&
		strcmp(element->author.middle_name, "Ivanov I. I.") == 0 &&
		strcmp(element->author.last_name, "Ivanov I. I.") == 0;
}

bool sea_by_date(Conference* element)
{
 return element->start.hours == element->finish.hours && element->finish.minutes > element->start.minutes + 16 ||
    element->start.hours < element->finish.hours && element->finish.minutes + 60 - element->start.minutes > 15;
}
